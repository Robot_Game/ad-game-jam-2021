using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISwitcherButton : Button
{

    protected override void OnEnable()
    {
        onClick.AddListener(Select);
        base.OnEnable();
    }
    protected override void OnDisable()
    {
        onClick.RemoveListener(Select);
        base.OnDisable();
    }
    public override void Select()
    {
        image.color = colors.selectedColor;

        UISwitcherButton[] buttons = transform.parent.GetComponentsInChildren<UISwitcherButton>();
        foreach (UISwitcherButton b in buttons)
        {
            if (b != this) b.Deselect();
        }
    }

    public void Deselect()
    {
        image.color = colors.normalColor;
    }


}
