using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISwitcher : MonoBehaviour
{
    private void Start()
    {
        Initialize();
    }
    protected virtual void Initialize()
    {
        SetSelectedButton(0);
    }

    protected virtual void SetSelectedButton(int buttonIndex, bool invokeOnClickEvent = false)
    {
        UISwitcherButton b = transform.GetChild(buttonIndex).GetComponent<UISwitcherButton>();
        if (b != null)
        {
            b.Select();
            if (invokeOnClickEvent) b.onClick?.Invoke();
        }

    }
}
