﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityExtensions
{
    /// <summary>
    /// Check if a Layer is inside a LayerMask
    /// </summary>
    /// <param name="mask">The LayerMask we want to check.</param>
    /// <param name="layer">The Layer we want to check.</param>
    /// <returns></returns>
    public static bool Contains(this LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    /// <summary>
    /// Whether a point is inside a collider
    /// </summary>
    /// <param name="collider"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    public static bool ContainsPoint(this Collider collider, Vector3 point)
    {
        return (collider.ClosestPoint(point) - point).sqrMagnitude < Mathf.Epsilon * Mathf.Epsilon;
    }

    /// <summary>
    /// Set a material's type to transparent
    /// </summary>
    /// <param name="mat"></param>
    public static void MakeMaterialTransparent(this Material mat)
    {
        mat.EnableKeyword("_BLENDMODE_ALPHA");
        mat.EnableKeyword("_SURFACE_TYPE_TRANSPARENT");
        mat.EnableKeyword("_ENABLE_FOG_ON_TRANSPARENT");
        mat.DisableKeyword("_BLENDMODE_ADD");
        mat.DisableKeyword("_BLENDMODE_PRE_MULTIPLY");
        mat.SetFloat("_SurfaceType", 1);
        mat.SetFloat("_RenderQueueType", 5);
        mat.SetFloat("_BlendMode", 0);
        mat.SetFloat("_AlphaCutoffEnable", 0);
        mat.SetFloat("_SrcBlend", 1f);
        mat.SetFloat("_DstBlend", 10f);
        mat.SetFloat("_AlphaSrcBlend", 1f);
        mat.SetFloat("_AlphaDstBlend", 10f);
        mat.SetFloat("_ZTestDepthEqualForOpaque", 4f);
        mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
    }
        /// <summary>
    /// Set a material's type to opaque
    /// </summary>
    /// <param name="mat"></param>
    public static void MakeMaterialOpaque(this Material mat)
    {
        mat.SetInt("_SurfaceType", (int)0);
        mat.SetInt("_RenderQueueType", (int)1);
        mat.SetFloat("_AlphaDstBlend", 0f);
        mat.SetFloat("_DstBlend", 0f);
        mat.SetFloat("_ZTestDepthEqualForOpaque", 3f);
        mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Geometry;
    }
}
