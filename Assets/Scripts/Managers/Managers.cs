using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-10)]
public class Managers : MonoBehaviour
{
    public static GameManager GameManager { get; private set; }
    public static UIManager UIManager { get; private set; }

    void Awake()
    {
        GameManager = FindObjectOfType<GameManager>();
        UIManager = FindObjectOfType<UIManager>();
    }
}
